/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, TouchableOpacity, ScrollView, Text, Alert
} from 'react-native'
import { ListItem, Avatar, Button, CheckBox } from 'react-native-elements'
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { DANH_SACH_THANH_VIEN_THAM_GIA } from '../../config/default';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';
var DATA = [
    {
        StaffID: 100,
        LoginUserID: "manh.tq",
        StaffName: "Trần Quang Mạnh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "anh.dv",
        StaffName: "Đinh Văn Anh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "linh.dv",
        StaffName: "Đinh Văn Linh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "khoa.mv",
        StaffName: "Mạc Văn Khoa",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "khoa.vanhoang",
        StaffName: "Hoàng Văn Khoa",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "anh.pv",
        StaffName: "Pham Van Anh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "linh.pt",
        StaffName: "Pham Thi Linh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },
    {
        StaffID: 100,
        LoginUserID: "dinh.ph",
        StaffName: "Phan Hoang Dinh",
        ImageFile: "http://img.bcdcnt.net/thumb?src=files/a6/49/3d/a6493d705a4c727f80cda05301c4af1c.jpg&w=728&h=409&zc=1"
    },

];
class Attendance extends Component<Props> {
    constructor(props) {
        super(props);
        self = this;
        this.state = {
            checked: this.onInit(),
            countNotAbsent:0,
        }
    }
    onInit = () => {
        let arr = [];
        DATA.map((value, index) => {
            arr.push({
                LoginUserID: value.LoginUserID,
                present: false,
            });
        })
        return arr;
    }
    sendList = () => {
        const {countNotAbsent,checked} = this.state;
        let res="Quân số "+countNotAbsent+"/"+checked.length+ " đồng chí.Bạn có muốn gửi kết quả không?";
        Alert.alert(
            'Thông báo',
            res,
            [
              { text: 'Huỷ', onPress: () => { }, style: 'cancel' },
              { text: 'Gửi kết quả', onPress: () => { 
                this.props.navigation.goBack();
                Alert.alert('Bạn đã gửi danh sách thành công')
               } },
            ],
            { cancelable: false }
          )
        
    }
    changeCheck = (id) => {
        let checked = this.state.checked;
        let countNotAbsent = checked[id].present ? this.state.countNotAbsent-1 : this.state.countNotAbsent+1;
        checked[id].present = !checked[id].present;
        this.setState({ checked , countNotAbsent})
    }
    render() {
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={'Điểm danh'}
                    onButton={() => this.props.navigation.goBack()}
                />

                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}
                    contentContainerStyle={{
                        alignItems: 'center',
                        paddingTop: 15 * STYLES.heightScreen / 896
                    }}
                >
                    {
                        DATA.map((value, index) => {
                            return (
                                <TouchableOpacity
                                    onPress={() => this.changeCheck(index)}
                                    style={styles.Cntcheckbox}>
                                    <View style={styles.avatar}>
                                        <Avatar
                                            rounded
                                            size="medium"
                                            source={{
                                                uri:
                                                    value.ImageFile
                                            }}
                                        />
                                    </View>

                                    <View style={styles.content}>
                                        <Text style={styles.text}>{"Tên: " + value.StaffName}</Text>
                                        <Text style={styles.text}>{"TK: " + value.LoginUserID}</Text>
                                        <View style={styles.cntInfo}>
                                            <View style={styles.cntSmall}>
                                                <Text style={styles.textSmall}>{5}</Text>
                                                <Text style={styles.textSmall}>{"Bỏ họp"}</Text>
                                            </View>
                                            <View style={styles.cntSmall}>
                                                <Text style={styles.textSmall}>{3}</Text>
                                                <Text style={styles.textSmall}>{"Vắng ĐD"}</Text>
                                            </View>
                                            <View style={styles.cntSmall}>
                                                <Text style={styles.textSmall}>{2}</Text>
                                                <Text style={styles.textSmall}>{"Sai QĐ"}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={styles.cntCheckbox}>
                                        <CheckBox
                                            checkedColor={Colors.green}
                                            checked={this.state.checked[index].present}
                                            onPress={() => this.changeCheck(index)}
                                        />
                                    </View>

                                </TouchableOpacity>
                            )
                        })
                    }
                    <Button
                        title="Gửi danh sách"
                        onPress={() => {
                            this.sendList();
                        }}
                        containerStyle={styles.btnSendList}
                        buttonStyle={styles.buttonSendList}
                    />
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(Attendance);