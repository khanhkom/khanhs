/**
 * This component to show account
 * @huanhtm
 */
import React, { Component } from 'react'
import {
    View, StyleSheet, Image, AsyncStorage, ScrollView
} from 'react-native'
import { ListItem } from 'react-native-elements'
import { connect } from 'react-redux';

//import config
import IMAGE from '../../assets/image';
import STYLES from '../../config/styles.config';
import Colors from '../../config/Colors';
import { QUAN_TRI_DANG_VIEN } from '../../config/default';
import styles from './styles'

//import common

import HeaderReal from '../../common/HeaderReal';

class Management extends Component<Props> {
    render() {
        return (
            <View style={styles.cntMain}>
                <HeaderReal
                    title={QUAN_TRI_DANG_VIEN}
                    onButton={() => this.props.navigation.goBack()}
                />
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1 }}>
                    <ListItem
                        title={'Điểm danh'}
                        leftIcon={{name:'event-note'}}
                        rightIcon={{name:'keyboard-arrow-right'}}
                        style={{marginVertical:5}}
                        onPress={()=>{
                            this.props.navigation.navigate('ListEvent');
                        }}
                    />
                    <ListItem
                        title={'Đảng phí'}
                        leftIcon={{name:'attach-money'}}
                        rightIcon={{name:'keyboard-arrow-right'}}
                        onPress={()=>{

                        }}
                    />
                </ScrollView>
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        MailBox: state.rootReducer.MailBox,
        Account: state.rootReducer.Account,
        DataExercises: state.rootReducer.DataExercises,
    };
}

export default connect(mapStateToProps, {
})(Management);